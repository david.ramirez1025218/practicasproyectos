package list;

import exceptions.EmptyListException;
import exceptions.IndexListException;
import exceptions.NonExistentElementException;

import java.lang.reflect.Field;
import java.util.Random;

public class MyLinkedList<E> {
    private Node<E> head;
    private int length = 0;
    private Field field;
    private String typeField;

    public MyLinkedList() {
        this.head = null;
    }

    public int size() {
        return this.length;
    }

    public boolean isEmpty() {
        return this.head == null;
    }

    public void addFirst(E info) {

        this.length++;

        if (isEmpty()) {
            this.head = new Node<>(info);
            return;
        }

        Node<E> newNode = new Node<>(info);

        newNode.setNext(this.head);

        this.head = newNode;

    }

    public void add(E info) {

        if (isEmpty()) {
            addFirst(info);
            return;
        }

        Node<E> tmp = this.head;

        while (tmp.getNext() != null) {
            tmp = tmp.getNext();
        }

        tmp.setNext(new Node<>(info));

        this.length++;

    }

    public void add(int idx, E info) throws IndexListException, EmptyListException {

        if (idx < 0 || idx >= size()) throw new IndexListException(size());

        if (isEmpty() || idx == 0) {
            addFirst(info);
            return;
        }

        Node<E> newNode = new Node<>(info);

        var tmp = getNode(idx - 1);

        newNode.setNext(tmp.getNext());

        tmp.setNext(newNode);

        this.length++;
    }

    public E get(int idx) throws IndexListException, EmptyListException {

        if (isEmpty()) throw new EmptyListException();

        if (idx < 0 || idx > size()) throw new IndexListException(size());

        if (idx == 0) return this.head.getInfo();

        var tmp = getNode(idx);

        return tmp.getInfo();

    }

    private Node<E> getNode(int idx) throws EmptyListException, IndexListException {

        if (isEmpty()) throw new EmptyListException();

        if (idx < 0 || idx > size()) throw new IndexListException(size());

        var tmp = this.head;

        for (int i = 0; i < idx; i++) tmp = tmp.getNext();

        return tmp;

    }

    public void update(int idx, E data) throws EmptyListException, IndexListException{

        if(isEmpty()) throw new EmptyListException();

        if(idx < 0 || idx > size()) throw new IndexListException(size());

        var tmp = getNode(idx);

        tmp.setInfo(data);

    }

    public E delete(int idx) throws IndexListException, EmptyListException {

        if (isEmpty()) throw new EmptyListException();

        if (idx < 0 || idx > size()) throw new IndexListException(size());

        E info;

        this.length--;

        if (idx != 0) {

            Node<E> tmp = getNode(idx - 1);

            info = tmp.getNext().getInfo();

            tmp.setNext(tmp.getNext().getNext());

            return info;

        }

        info = this.head.getInfo();

        this.head = this.head.getNext();

        return info;

    }

    public void deleteAll() {
        this.head = null;
        this.length = 0;
    }

    public void print() throws EmptyListException {

        if (isEmpty()) throw new EmptyListException();

        var tmp = this.head;

        System.out.println("List[" + size() + "]");
        System.out.println("{");
        while (tmp != null) {
            System.out.println(tmp.getInfo().toString());
            tmp = tmp.getNext();
        }
        System.out.println("}");

    }

    public void printLineal() throws EmptyListException {

        if (isEmpty()) throw new EmptyListException();

        var tmp = this.head;

        System.out.println("List[" + size() + "]");
        System.out.print("{");
        while (tmp != null) {
            System.out.print(tmp.getInfo().toString() +" ");
            tmp = tmp.getNext();
        }
        System.out.println("}");

    }
    public E[] toArray() {

        E[] array = null;

        if(this.size() == 0) return array;

        array = (E[]) java.lang.reflect.Array.newInstance(this.head.getInfo().getClass(), size());

        Node<E> tmp = this.head;

        for (int i = 0; i < size(); i++) {
            array[i] = tmp.getInfo();
            tmp = tmp.getNext();
        }

        return array;

    }

    public void toList(E[] array) {

        this.deleteAll();

        for (E e : array) this.add(e);

    }

    private void getAttribute(String attribute) {

        Field[] fields = this.head.getInfo().getClass().getDeclaredFields();

        for (var fld : fields) if (fld.getName().equals(attribute)) this.field = fld;

        field.setAccessible(true);

        this.typeField = field.getType().getSimpleName();

    }

    private void resetAttribute() {
        field = null;
        typeField = null;
    }

    private int compare(E e1, E e2) throws IllegalAccessException {

        if (typeField.equals("int") || typeField.equals("double")) {

            return Double.compare(((Number) field.get(e1)).doubleValue(), ((Number) field.get(e2)).doubleValue());

        } else {

            return Integer.compare(field.get(e1).toString().compareToIgnoreCase(field.get(e2).toString()), 0);

        }

    }

    private int compareS(E e1, Object e2) throws IllegalAccessException {

        if (typeField.equals("int") || typeField.equals("double")) {

            return Double.compare(((Number) field.get(e1)).doubleValue(), ((Number) e2).doubleValue());

        } else {

            return Integer.compare(field.get(e1).toString().compareToIgnoreCase(e2.toString()), 0);

        }

    }

    public void shuffle() throws EmptyListException {

        var tmpArr = this.toArray();

        Random random = new Random();

        for (int i = 0; i < tmpArr.length; i++) {

            int randomNum = random.nextInt(tmpArr.length - 1);

            var tmp = tmpArr[i];

            tmpArr[i] = tmpArr[randomNum];

            tmpArr[randomNum] = tmp;

        }

        this.toList(tmpArr);

    }

    private int partition(E[] arr, int low, int high, boolean typeSort) throws IllegalAccessException {

        var pivot = arr[high];

        int i = (low - 1);

        for (int j = low; j < high; j++) {

            if (typeSort ? compare(arr[j], pivot) <= 0 : compare(arr[j], pivot) >= 0) {

                i++;
                var tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = tmp;

            }

        }

        var tmp = arr[i + 1];

        arr[i + 1] = arr[high];

        arr[high] = tmp;

        return i + 1;

    }

    private void quick(E[] arr, int low, int high, boolean typeSort) throws IllegalAccessException {

        if (low < high) {

            int pi = partition(arr, low, high, typeSort);

            quick(arr, low, pi - 1, typeSort);

            quick(arr, pi + 1, high, typeSort);

        }

    }

    private void merge(E[] arr, int l, int m, int r, boolean typeSort) throws IllegalAccessException {

        int n1 = m - l + 1;
        int n2 = r - m;

        E[] L = (E[]) java.lang.reflect.Array.newInstance(this.head.getInfo().getClass(), n1);

        E[] R = (E[]) java.lang.reflect.Array.newInstance(this.head.getInfo().getClass(), n2);

        for (int i = 0; i < n1; ++i) L[i] = arr[l + i];

        for (int j = 0; j < n2; ++j) R[j] = arr[m + 1 + j];

        int i = 0, j = 0;

        int k = l;

        while (i < n1 && j < n2) {

            if (typeSort ? compare(L[i], R[j]) <= 0 : compare(L[i], R[j]) >= 0) {

                arr[k] = L[i];
                i++;

            } else {

                arr[k] = R[j];
                j++;

            }

            k++;
        }

        while (i < n1) {
            arr[k] = L[i];
            i++;
            k++;
        }

        while (j < n2) {
            arr[k] = R[j];
            j++;
            k++;
        }

    }

    private void sortMerge(E[] arr, int l, int r, boolean typeSort) throws IllegalAccessException {

        if (l < r) {

            int m = l + (r - l) / 2;

            sortMerge(arr, l, m, typeSort);

            sortMerge(arr, m + 1, r, typeSort);

            merge(arr, l, m, r, typeSort);

        }

    }

    public void mergeSort(String attribute, boolean typeSort) throws EmptyListException, IllegalAccessException {

        if (isEmpty()) throw new EmptyListException();

        if (size() == 1) return;

        getAttribute(attribute);

        var tmp = this.toArray();

        sortMerge(tmp, 0, (tmp.length - 1), typeSort);

        resetAttribute();

        this.toList(tmp);

    }

    public void quickSort(String attribute, boolean typeSort) throws EmptyListException, IllegalAccessException {

        if (isEmpty()) throw new EmptyListException();

        if (size() == 1) return;

        getAttribute(attribute);

        var tmp = this.toArray();

        quick(tmp, 0, (tmp.length - 1), typeSort);

        resetAttribute();

        this.toList(tmp);

    }


    private int binary(E[] arr, Object element, int low, int high) throws IllegalAccessException {

        if (high >= low) {

            int mid = low + (high - low) / 2;

            //check if mid-element is searched element
            if (compareS(arr[mid], element) == 0) return mid;

            //Search the left half of mid
            if (compareS(arr[mid], element) > 0) return binary(arr, element, low, mid - 1);

            //Search the right half of mid
            return binary(arr, element, mid + 1, high);

        }

        return -1;
    }

    public E binarySearch(String attribute, Object info) throws EmptyListException, IllegalAccessException, NonExistentElementException, IndexListException {

        if (isEmpty()) throw new EmptyListException();

        var tmpArray = this.toArray();

        getAttribute(attribute);

        int idx = binary(tmpArray, info, 0, tmpArray.length - 1);

        resetAttribute();

        if (idx == -1) throw new NonExistentElementException();

        return this.get(idx);
    }

    public MyLinkedList<E> linearSearch(String attribute, Object info) throws EmptyListException, IllegalAccessException, NonExistentElementException, IndexListException {

        MyLinkedList<E> result = new MyLinkedList<>();

        var tmpArray = toArray();

        getAttribute(attribute);

        int idx = binary(tmpArray, info, 0, tmpArray.length - 1);

        if (idx == -1) throw new NonExistentElementException();

        int tmpIdx = 0;

        for (int i = idx; i >= 0; i--) {
            if (compareS(this.get(i), info) != 0) break;
            tmpIdx = i;
        }

        for (int i = tmpIdx; i < this.size(); i++) {
            if (compareS(this.get(i), info) != 0) break;
            result.add(this.get(i));
        }

        resetAttribute();

        return result;
    }

}
