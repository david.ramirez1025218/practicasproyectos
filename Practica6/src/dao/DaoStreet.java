package dao;

import java.io.IOException;
import model.Street;

public class DaoStreet extends DaoAdap<Street>{
    private Street street;
    
    public DaoStreet(){
        super(Street.class);
    }
    
    public void connect(String name, int start, int end, double length){
        
        street = new Street();
        street.setName(name);
        street.setStart(start);
        street.setEnd(end);
        street.setLength(length);
        
        System.out.println(street);
        
        try {
            save(street);
        } catch (IOException ex) {
            System.out.println("Error: "+ ex.getMessage());
        }
        
        street = null;
    }
    
}
