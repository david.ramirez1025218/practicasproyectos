package dao;

import exceptions.EmptyListException;
import exceptions.IndexListException;
import exceptions.NonExistentElementException;
import java.io.IOException;
import model.Neighborhood;

public class DaoNeighborhood extends DaoAdap<Neighborhood>{
    
    private Neighborhood neighbor;
    
    public DaoNeighborhood() {
        super(Neighborhood.class);
    }
    
    public void create(String name, double latitude, double longitude){
        neighbor = new Neighborhood();
        neighbor.setId(generateId());
        neighbor.setName(name);
        neighbor.setLatitude(latitude);
        neighbor.setLongitude(longitude);
        try {
            save(neighbor);
        } catch (IOException ex) {
            System.out.println("Error: "+ ex);
        }
        neighbor = null;
    }
    
    public Neighborhood searchName(String name) throws IllegalAccessException, EmptyListException, NonExistentElementException, IndexListException{
        
        var list = getAll();
        
        list.mergeSort("name", true);
        
        return list.binarySearch("name", name);
    }
    
}
