package graph;

import exceptions.GraphSizeException;

public class UndirectedLabelGraph<E> extends DirectedLabelGraph<E>{

    public UndirectedLabelGraph(int numVertices) {
        super(numVertices);
    }

    public void addEdge(int i, int j, double weight) throws GraphSizeException {

        if(i > numVertices || j > numVertices) throw new GraphSizeException();

        if(edgeExist(i, j)) return;

        neighbors[i].add(new Adjacency(j, weight));
        neighbors[j].add(new Adjacency(i, weight));

        numEdges += 2;

    }

    public void addLabelEdge(E i, E j, double weight) throws GraphSizeException{
        this.addEdge(getNumVertices(i), getNumVertices(j), weight);
        this.addEdge(getNumVertices(i), getNumVertices(j), weight);
    }
}
