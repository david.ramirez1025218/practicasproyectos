package graph;

import model.Neighborhood;

public class Util {
    
    private static final double RADIUS = 6371;
    /*
     * calculate the time of a distance in seconds
     * speed(meter per second)
     * distance(meter)
     */
    public static double time(int speed, int distance) {
        return (double) distance / speed;
    }

    /*
     * calculate the distance from one point to another
     * using geographic coordinate
     */
    public static double distance(Neighborhood point1, Neighborhood point2) {

        double phi1 = Math.toRadians(point1.getLatitude());
        double phi2 = Math.toRadians(point2.getLatitude());
        
        double deltaPhi = Math.toRadians(point2.getLatitude() - point1.getLatitude());
        double deltaLambda = Math.toRadians(point2.getLongitude() - point1.getLongitude());

        double a = Math.sin(deltaPhi / 2) * Math.sin(deltaPhi / 2)
                + Math.cos(phi1) * Math.cos(phi2) * Math.sin(deltaLambda / 2) * Math.sin(deltaLambda / 2);

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return RADIUS * c;
    }

}
