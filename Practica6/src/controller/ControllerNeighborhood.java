package controller;

import dao.DaoStreet;
import exceptions.EmptyListException;
import exceptions.GraphCycleException;
import exceptions.GraphSizeException;
import exceptions.IndexListException;
import exceptions.NonExistentElementException;
import exceptions.NonExistentPathException;
import graph.UndirectedLabelGraph;
import graph.Util;
import java.text.DecimalFormat;
import list.MyLinkedList;
import model.Neighborhood;

public class ControllerNeighborhood {

    private MyLinkedList<Neighborhood> neighbors;
    private UndirectedLabelGraph<Neighborhood> graph;

    public ControllerNeighborhood() {
        refresh();
    }

    private void labelGraph() {

        graph = new UndirectedLabelGraph<>(neighbors.size());

        var tmp = neighbors.toArray();

        for (int i = 0; i < tmp.length; i++) {
            graph.labelVertex(i + 1, tmp[i]);
        }

        try {
            loadEdges();
        } catch (GraphSizeException ex) {
            System.out.println("Error: " + ex.getMessage());
        }

    }

    private void loadEdges() throws GraphSizeException {

        var edges = new DaoStreet().getAll().toArray();

        if (edges == null) {
            return;
        }

        for (var e : edges) {
            graph.addEdge(e.getStart(), e.getEnd(), e.getLength());
        }
    }

    private double distance(int n1, int n2) throws IndexListException, EmptyListException {

        var ngb1 = neighbors.get(n1);
        var ngb2 = neighbors.get(n2);

        DecimalFormat decimalFormat = new DecimalFormat("#.###");

        String roundedNumber = decimalFormat.format(Util.distance(ngb1, ngb2));

        return Double.parseDouble(roundedNumber.replace(',', '.'));
    }

    public void createStreet(String name, String neighbor1, String neighbor2) {
        try {

            var start = new dao.DaoNeighborhood().searchName(neighbor1).getId();

            var end = new dao.DaoNeighborhood().searchName(neighbor2).getId();

            double length = distance(start - 1, end - 1);

            System.out.println(start + " -> " + end + "[" + length + "]");

            new dao.DaoStreet().connect(name, start, end, length);

        } catch (IllegalAccessException | EmptyListException | NonExistentElementException | IndexListException ex) {
            System.out.println("Error: " + ex.getMessage());
        }

    }

    public UndirectedLabelGraph<Neighborhood> path(String n1, String n2, int tipo) {

        if (tipo == 0) {
            return floyd(n1, n2);
        }

        if (tipo == 1) {
            return bellman(n1, n2);
        }

        return null;
    }

    private UndirectedLabelGraph<Neighborhood> floyd(String n1, String n2) {

        try {
            int i = new dao.DaoNeighborhood().searchName(n1).getId();
            int j = new dao.DaoNeighborhood().searchName(n2).getId();

            return showPath(graph.floyd(i, j), j);
        } catch (IllegalAccessException | EmptyListException | NonExistentElementException | IndexListException | GraphCycleException | NonExistentPathException ex) {
            System.out.println("Error: "+ ex.getMessage());
        }
        return null;
    }

    private UndirectedLabelGraph<Neighborhood> bellman(String n1, String n2) {

        try {

            int i = new dao.DaoNeighborhood().searchName(n1).getId();

            int j = new dao.DaoNeighborhood().searchName(n2).getId();

            return showPath(graph.bellmanFord(i, j), j);

        } catch (IllegalAccessException | EmptyListException | NonExistentElementException | IndexListException ex) {
            System.out.println("Error: " + ex.getMessage());
        }
        return null;
    }

    private UndirectedLabelGraph<Neighborhood> showPath(MyLinkedList path, int dest) {

        if (path == null) {
            return null;
        }

        path.add(dest);

        var pathGraph = new UndirectedLabelGraph<Neighborhood>(path.size());

        System.out.println(pathGraph.numVertices());

        var tmp = path.toArray();

        for (int i = 0; i < tmp.length; i++) {

            var label = graph.getLabel((int) tmp[i]);

            pathGraph.labelVertex((i + 1), label);

            if ((i + 1) == tmp.length) {
                break;
            }

            try {
                pathGraph.addEdge((i + 1), (i + 2), graph.edgeWeight((int) tmp[i], (int) tmp[i + 1]));
            } catch (GraphSizeException ex) {
                System.out.println("Error : " + ex.getMessage());
            }

        }
        
        return pathGraph;
    }

    public void refresh() {
        neighbors = new dao.DaoNeighborhood().getAll();
        labelGraph();
    }

    public MyLinkedList<Neighborhood> getNeighbors() {
        return neighbors;
    }

    public UndirectedLabelGraph<Neighborhood> getGraph() {
        return graph;
    }

}
