package queue;

import list.Node;

public class MyQueue<E> {
    private Node<E> head;
    private int length;

    public MyQueue(){
        this.head = null;
    }

    public void offer(E data){

        length++;

        if(isEmpty()){
            head = new Node<>(data);
            return;
        }

        Node<E> tmp = this.head;

        while (tmp.getNext() != null) tmp = tmp.getNext();

        tmp.setNext(new Node<>(data));

    }

    public E peek(){
        return head.getInfo();
    }

    public E poll(){

        length--;

        if(isEmpty()) return null;

        var tmp = head;

        head = head.getNext();

        return tmp.getInfo();

    }
    
    public void print(){
        
        var tmp = head;
        System.out.println("Queue");
        while(tmp != null){
            System.out.println(tmp.getInfo());
            tmp = tmp.getNext();
        }
        
    }

    public int size(){
        return length;
    }

    public boolean isEmpty(){
        return head == null;
    }
}